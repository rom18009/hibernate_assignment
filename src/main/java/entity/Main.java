package entity;

import java.util.*;

/** https://www.tutorialspoint.com/hibernate/index.htm
 * https://blog.jetbrains.com/idea/2021/02/creating-a-simple-jpa-application/
 * */
public class Main {
    public static void main(String[] args) {
        menu();
    }

    public static void menu() {
        int i = 1;
        TestDAO t = TestDAO.getInstance();

        System.out.println("What do you want to do?");
        System.out.println("1 - List entrees available");
        System.out.println("2 - Add an entree");
        System.out.println("3 - Update entree name");
        System.out.println("4 - Remove a entree");
        System.out.println("0 - Exit");

        Scanner in = new Scanner(System.in);
        int num = in.nextInt();

        switch (num) {

            case 1:
                listEntrees(t);
                break;
            case 2:
                addEntree(t);
                break;
            case 3:
                updateEntree(t);
                break;
            case 4:
                removeEntree(t);
                break;
            case 0:
                i = 0;
                break;
        }

        if (i == 1) {
            menu();
        } else {
            System.out.println("Exiting...");
            System.exit(1);
        }
    }

    public static void addEntree(TestDAO t) {
        Scanner input = new Scanner(System.in);
        System.out.println("Type the name of the entree:");
        String name = input.nextLine();
        Orders entree = new Orders();
        entree.setName(name);
        if (t.addEntree(entree)) {
            listEntrees(t);
        } else {
            System.out.println("Entree not added");
        }
    }

    public static void listEntrees(TestDAO t) {
        System.out.println("-------------------- Entrees Available --------------------");
        for (Orders o : t.getEntrees()) {
            System.out.println(o.getId() + " - " + o.getName());
        }
    }

    public static void updateEntree(TestDAO t) {
        System.out.println("-------------------- Entrees Available --------------------");
        for (Orders o : t.getEntrees()) {
            System.out.println(o.getId() + " - " + o.getName());
        }
        Scanner input1 = new Scanner(System.in);
        Scanner input2 = new Scanner(System.in);
        System.out.println("Type the ID of the entree:");
        try { int id = input1.nextInt();
        System.out.println("Type the new name of the entree:");
        String newName = input2.nextLine();
         if (t.updateEntree(id, newName)) {
            listEntrees(t);
        } } catch (NullPointerException n)  {
            System.out.println("Entree not added" + n);
            throw new IllegalArgumentException(n);
        }
    }

    public static void removeEntree(TestDAO t) {
        Scanner input = new Scanner(System.in);
        System.out.println("Type the ID of the entree you want to remove:");
        int id = input.nextInt();
        if (t.removeEntree(id)) {
            listEntrees(t);
        } else {
            System.out.println("Entree not updated");
        }
    }
}

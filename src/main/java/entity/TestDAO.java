package entity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

/** Modified from Brother Tuckett's example video/code
 * Also used the following youtube videos:
 *   https://www.youtube.com/watch?v=Bld3644bIAo&ab_channel=BrianFraser
 *   https://www.youtube.com/watch?v=QJddHc41xrM&ab_channel=IntelliJIDEAbyJetBrains
 * */
public class TestDAO {

    EntityManagerFactory factory = null;
    EntityManager entityManager = null;

    private static TestDAO single_instance = null;

    private TestDAO() {
        factory = HibernateUtils.getEntityManager();
    }

    /** This is what makes this class a singleton. You use this class to get an instance of the class. */
    public static TestDAO getInstance() {
        if (single_instance == null) {
            single_instance = new TestDAO();
        }
        return single_instance;
    }

    /** Used to get more than one entrée from the database. */
    public List<Orders> getEntrees() {

        try {
            entityManager = factory.createEntityManager();
            entityManager.getTransaction().begin();
            String sql = "from Orders";
            List<Orders> cs = (List<Orders>) entityManager.createQuery(sql).getResultList();
            entityManager.getTransaction().commit();
            return cs;

        } catch ( Exception e ) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            entityManager.getTransaction().rollback();
            return null;
        } finally {
            entityManager.close();
        }

    }

    /** Used to get a single customer from database if I were using the customer table*/
    public Orders getCustomer(int id) {

        try {
            entityManager = factory.createEntityManager();
            entityManager.getTransaction().begin();
            String sql = "from Orders where id=" + Integer.toString(id);
            Orders c = (Orders) entityManager.createQuery(sql).getSingleResult();
            entityManager.getTransaction().commit();
            return c;

        } catch ( Exception e ) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            entityManager.getTransaction().rollback();
            return null;
        } finally {
            entityManager.close();
        }
    }

    /** Used to add an entrée into the database */
    public boolean addEntree(Orders entree) {
        try {
            entityManager = factory.createEntityManager();
            entityManager.getTransaction().begin();
            entityManager.persist(entree);
            entityManager.getTransaction().commit();
            return true;

        } catch ( Exception e ) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
    }

    /** Used to remove an entrée from the database based on the id*/
    public boolean removeEntree(int id) {
        try {
            entityManager = factory.createEntityManager();
            entityManager.getTransaction().begin();
            Orders entree = entityManager.find(Orders.class, id);
            entityManager.remove(entree);
            entityManager.getTransaction().commit();
            return true;

        } catch ( Exception e ) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
    }

    /** Used to update an entrée in the database based on the id*/
    public boolean updateEntree(int id, String name) {
        try {
            entityManager = factory.createEntityManager();
            entityManager.getTransaction().begin();
            Orders entree = entityManager.find(Orders.class, id);
            entree.setName(name);
            entityManager.merge(entree);
            entityManager.getTransaction().commit();
            return true;

        } catch ( Exception e ) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
    }

}

package entity;

import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;

/** Created from the persistence database mapping */
/** https://www.youtube.com/watch?v=KHohVibqePw&ab_channel=Arseniy */
@Entity
@Table(name = "orders_table")
public class Orders {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(generator = "incrementor")
    @GenericGenerator(name = "incrementor", strategy = "increment")
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

package entity;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/** Modified from Brother Tuckett's example video/code */
public class HibernateUtils {

    private static final EntityManagerFactory entityManagerFactory = buildEntityManagerFactory();

    private static EntityManagerFactory buildEntityManagerFactory() {
        try {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        return entityManagerFactory;
        } catch (Throwable e){
            System.out.println("Initial entityManagerFactory creation failed" + e);
            throw new ExceptionInInitializerError(e);
        }
    }

    public static EntityManagerFactory getEntityManager() {
        return entityManagerFactory;
    }

    public static void shutdown() {
        getEntityManager().close();
    }

}
